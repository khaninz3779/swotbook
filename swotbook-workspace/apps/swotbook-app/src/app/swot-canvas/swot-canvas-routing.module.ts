import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SwotCanvasMainComponent } from './swot-canvas-main/swot-canvas-main.component';

const routes: Routes = [
  { path: '', component: SwotCanvasMainComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SwotCanvasRoutingModule {}
