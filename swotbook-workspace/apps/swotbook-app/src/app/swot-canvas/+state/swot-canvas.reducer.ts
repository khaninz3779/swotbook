import { Action } from '@ngrx/store';
import {
  SwotCanvasActions,
  SwotCanvasActionTypes
} from './swot-canvas.actions';

/**
 * Interface for the 'SwotCanvas' data used in
 *  - SwotCanvasState, and
 *  - swotCanvasReducer
 */
export interface SwotCanvasData {}

/**
 * Interface to the part of the Store containing SwotCanvasState
 * and other information related to SwotCanvasData.
 */
export interface SwotCanvasState {
  readonly swotCanvas: SwotCanvasData;
}

export const initialState: SwotCanvasData = {};

export function swotCanvasReducer(
  state = initialState,
  action: SwotCanvasActions
): SwotCanvasData {
  switch (action.type) {
    case SwotCanvasActionTypes.SwotCanvasAction:
      return state;

    case SwotCanvasActionTypes.SwotCanvasLoaded: {
      return { ...state, ...action.payload };
    }

    default:
      return state;
  }
}
