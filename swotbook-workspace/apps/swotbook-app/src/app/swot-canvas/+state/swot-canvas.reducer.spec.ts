import { SwotCanvasLoaded } from './swot-canvas.actions';
import { swotCanvasReducer, initialState } from './swot-canvas.reducer';

describe('swotCanvasReducer', () => {
  it('should work', () => {
    const action: SwotCanvasLoaded = new SwotCanvasLoaded({});
    const actual = swotCanvasReducer(initialState, action);
    expect(actual).toEqual({});
  });
});
