import { Action } from '@ngrx/store';

export enum SwotCanvasActionTypes {
  SwotCanvasAction = '[SwotCanvas] Action',
  LoadSwotCanvas = '[SwotCanvas] Load Data',
  SwotCanvasLoaded = '[SwotCanvas] Data Loaded'
}

export class SwotCanvas implements Action {
  readonly type = SwotCanvasActionTypes.SwotCanvasAction;
}
export class LoadSwotCanvas implements Action {
  readonly type = SwotCanvasActionTypes.LoadSwotCanvas;
  constructor(public payload: any) {}
}

export class SwotCanvasLoaded implements Action {
  readonly type = SwotCanvasActionTypes.SwotCanvasLoaded;
  constructor(public payload: any) {}
}

export type SwotCanvasActions = SwotCanvas | LoadSwotCanvas | SwotCanvasLoaded;
