import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import {
  SwotCanvasActions,
  SwotCanvasActionTypes,
  LoadSwotCanvas,
  SwotCanvasLoaded
} from './swot-canvas.actions';
import { SwotCanvasState } from './swot-canvas.reducer';
import { DataPersistence } from '@nrwl/nx';

@Injectable()
export class SwotCanvasEffects {
  @Effect()
  effect$ = this.actions$.ofType(SwotCanvasActionTypes.SwotCanvasAction);

  @Effect()
  loadSwotCanvas$ = this.dataPersistence.fetch(
    SwotCanvasActionTypes.LoadSwotCanvas,
    {
      run: (action: LoadSwotCanvas, state: SwotCanvasState) => {
        return new SwotCanvasLoaded(state);
      },

      onError: (action: LoadSwotCanvas, error) => {
        console.error('Error', error);
      }
    }
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<SwotCanvasState>
  ) {}
}
