import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';
import { DataPersistence } from '@nrwl/nx';
import { hot } from '@nrwl/nx/testing';

import { SwotCanvasEffects } from './swot-canvas.effects';
import { LoadSwotCanvas, SwotCanvasLoaded } from './swot-canvas.actions';

import { Observable } from 'rxjs/Observable';

describe('SwotCanvasEffects', () => {
  let actions$: Observable<any>;
  let effects$: SwotCanvasEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({})],
      providers: [
        SwotCanvasEffects,
        DataPersistence,
        provideMockActions(() => actions$)
      ]
    });

    effects$ = TestBed.get(SwotCanvasEffects);
  });

  describe('someEffect', () => {
    it('should work', () => {
      actions$ = hot('-a-|', { a: new LoadSwotCanvas({}) });
      expect(effects$.loadSwotCanvas$).toBeObservable(
        hot('-a-|', { a: new SwotCanvasLoaded({}) })
      );
    });
  });
});
