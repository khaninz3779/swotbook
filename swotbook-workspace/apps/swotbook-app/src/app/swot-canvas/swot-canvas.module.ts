import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwotCanvasRoutingModule } from './swot-canvas-routing.module';
import { SwotCanvasMainComponent } from './swot-canvas-main/swot-canvas-main.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  swotCanvasReducer,
  initialState as swotCanvasInitialState
} from './+state/swot-canvas.reducer';
import { SwotCanvasEffects } from './+state/swot-canvas.effects';
import { MatButtonModule, MatToolbarModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    SwotCanvasRoutingModule,
    StoreModule.forFeature('swotCanvas', swotCanvasReducer, {
      initialState: swotCanvasInitialState
    }),
    EffectsModule.forFeature([SwotCanvasEffects]),
    MatButtonModule,
    MatToolbarModule
  ],
  declarations: [SwotCanvasMainComponent],
  providers: [SwotCanvasEffects]
})
export class SwotCanvasModule {}
