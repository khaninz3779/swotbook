import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginMainComponent } from './login-main/login-main.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  loginReducer,
  initialState as loginInitialState
} from './+state/login.reducer';
import { LoginEffects } from './+state/login.effects';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    StoreModule.forFeature('login', loginReducer, {
      initialState: loginInitialState
    }),
    EffectsModule.forFeature([LoginEffects]),
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  declarations: [LoginMainComponent],
  providers: [LoginEffects]
})
export class LoginModule {}
