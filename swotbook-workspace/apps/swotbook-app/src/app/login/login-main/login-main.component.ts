import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login-main',
  templateUrl: './login-main.component.html',
  styleUrls: ['./login-main.component.css']
})
export class LoginMainComponent implements OnInit {
  constructor(
    public afAuth: AngularFireAuth
  ) {}

  ngOnInit() {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        console.log(user.uid);
        // ...
      } else {
        // User is signed out.
        // ...
        console.log('no user')
      }
    });

  }

  login(){

    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(()=> {
      console.log(this.afAuth);
    }).catch(err=> console.log(err))
  }

  logout(){

    this.afAuth.auth.signOut().then(()=>{
      console.log(this.afAuth);
    }).catch( err => console.log(err));

  }
}


