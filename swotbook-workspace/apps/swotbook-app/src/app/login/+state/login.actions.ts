import { Action } from '@ngrx/store';

export enum LoginActionTypes {
  LoginAction = '[Login] Action',
  LoadLogin = '[Login] Load Data',
  LoginLoaded = '[Login] Data Loaded'
}

export class Login implements Action {
  readonly type = LoginActionTypes.LoginAction;
}
export class LoadLogin implements Action {
  readonly type = LoginActionTypes.LoadLogin;
  constructor(public payload: any) {}
}

export class LoginLoaded implements Action {
  readonly type = LoginActionTypes.LoginLoaded;
  constructor(public payload: any) {}
}

export type LoginActions = Login | LoadLogin | LoginLoaded;
