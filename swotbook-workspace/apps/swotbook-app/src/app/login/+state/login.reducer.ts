import { Action } from '@ngrx/store';
import { LoginActions, LoginActionTypes } from './login.actions';

/**
 * Interface for the 'Login' data used in
 *  - LoginState, and
 *  - loginReducer
 */
export interface LoginData {}

/**
 * Interface to the part of the Store containing LoginState
 * and other information related to LoginData.
 */
export interface LoginState {
  readonly login: LoginData;
}

export const initialState: LoginData = {};

export function loginReducer(
  state = initialState,
  action: LoginActions
): LoginData {
  switch (action.type) {
    case LoginActionTypes.LoginAction:
      return state;

    case LoginActionTypes.LoginLoaded: {
      return { ...state, ...action.payload };
    }

    default:
      return state;
  }
}
